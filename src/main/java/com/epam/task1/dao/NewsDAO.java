package com.epam.task1.dao;

import java.util.List;

import com.epam.task1.dao.criteria.SearchCriteria;
import com.epam.task1.domain.News;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;

/**
 * Provides access to News entities into database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface NewsDAO {
	/**
	 * Adds News entity.
	 * 
	 * @param News
	 *            entity.
	 * @throws DAOException
	 */

	Long add(News news) throws DAOException;

	/**
	 * Removes News entity with specified id.
	 * 
	 * @param News
	 *            entity id.
	 * @throws DAOException
	 */

	void removeById(Long newsId) throws DAOException;

	/**
	 * Updates News entity.
	 * 
	 * @param News
	 *            entity.
	 * @throws DAOException
	 */

	void update(News news) throws DAOException;

	/**
	 * Fetches list of News entities.
	 * 
	 * @return list of News entities.
	 * @throws DAOException
	 */
	List<News> fetchAll() throws DAOException;

	/**
	 * Fetches News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @return News entity
	 * @throws DAOException
	 */
	News fetchById(Long newsId) throws DAOException;

	/**
	 * Counts all News entities stored in database.
	 * 
	 * @return News entities amount.
	 * @throws DAOException
	 */
	long countNews() throws DAOException;

	/**
	 * Adds connection between News entity with specified id and Author entity
	 * with specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @param authorId
	 *            Author entity id
	 * @throws DAOException
	 */
	void addAuthorToNews(Long newsId, Long authorId) throws DAOException;

	/**
	 * Removes connection between News entity with specified id and Author
	 * entity.
	 * 
	 * @param newsId
	 *            News entity id
	 * @throws DAOException
	 */
	void removeAuthorRelationByNewsId(Long newsId) throws DAOException;

	/**
	 * Adds connection between News entity with specified id and all Tag
	 * entities from the list.
	 * 
	 * @param newsId
	 *            News entity id
	 * @param tagsList
	 *            list of tags
	 * @throws DAOException
	 */
	void addTagsToNews(Long newsId, List<Tag> tagsList) throws DAOException;

	/**
	 * Removes connections between News entity with such id and all tags.
	 * 
	 * @param newsId
	 *            news entity id
	 * @throws DAOException
	 */
	void removeTagRelationsByNewsId(Long newsId) throws DAOException;

	/**
	 * Fetches News entities selected by SearchCriteria object which contains
	 * list of tags and list of authors. If one of lists is empty or null it
	 * will not be include into searching query.
	 * 
	 * @param sc
	 *            SearchCriteria entity
	 * @return list of news entities
	 * @throws DAOException
	 */
	List<News> fetchByCriteria(SearchCriteria sc) throws DAOException;

	/**
	 * Fetches News entities list which are sorted by comments amount
	 * descending. If there's no entities returns empty list.
	 * 
	 * @return News entities list
	 * @throws DAOException
	 */
	List<News> fetchSortedByComments() throws DAOException;

}
