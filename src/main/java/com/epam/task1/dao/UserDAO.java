package com.epam.task1.dao;

import com.epam.task1.domain.User;
import com.epam.task1.exception.DAOException;

/**
 * Provides access to User entities into database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface UserDAO {
	/**
	 * Adds User entity into database. Returns user entity id.
	 * 
	 * @param user
	 *            entity
	 * @return auto-generated key
	 * @throws DAOException
	 */

	Long add(User user) throws DAOException;

	/**
	 * Removes User entity by it's id.
	 * 
	 * @param user
	 *            entity id
	 * @throws DAOException
	 */

	void removeById(Long userId) throws DAOException;

	/**
	 * Updates User entity
	 * 
	 * @param user
	 *            entity
	 * @throws DAOException
	 */

	void update(User user) throws DAOException;

	/**
	 * Returns User entity with such login. If there's no entity with such login
	 * returns null.
	 * 
	 * @param login
	 *            user login
	 * @return user entity
	 * @throws DAOException
	 */
	User fetchByLogin(String login) throws DAOException;

	/**
	 * Returns User entity with such id. If there's no entity with such id
	 * returns null.
	 * 
	 * @param userId
	 * @return
	 * @throws DAOException
	 */
	User fetchById(Long userId) throws DAOException;
}
