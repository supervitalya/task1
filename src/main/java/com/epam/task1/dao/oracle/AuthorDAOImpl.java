package com.epam.task1.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.task1.dao.AuthorDAO;
import com.epam.task1.domain.Author;
import com.epam.task1.exception.DAOException;

@Component
public class AuthorDAOImpl implements AuthorDAO {

	public static final String SQL_ADD_AUTHOR = "insert into AUTHORS(AUTH_NAME, AUTH_EXPIRED) values(?,?)";
	public static final String SQL_UPDATE_AUTHOR = "update AUTHORS set AUTH_NAME=?,AUTH_EXPIRED=? where AUTH_ID=?";
	public static final String SQL_FETCH_ALL_AUTHORS = "select AUTH_ID,AUTH_NAME, AUTH_EXPIRED from AUTHORS";
	public static final String SQL_FETCH_AUTHOR_BY_NAME = "select  AUTH_ID, AUTH_EXPIRED from "
			+ "AUTHORS where AUTH_NAME =? ";
	public static final String SQL_FETCH_AUTHOR_BY_ID = "select  AUTH_NAME, AUTH_EXPIRED from "
			+ "AUTHORS where AUTH_ID =? ";
	public static final String SQL_FETCH_AUTHOR_BY_NEWS_ID = "select  AUTH_ID, AUTH_NAME, AUTH_EXPIRED"
			+ " from NEWS_AUTHORS join AUTHORS on NA_AUTHOR_ID = AUTH_ID " + "where NEWS_AUTHORS.NA_NEWS_ID=?";
	public static final String SQL_REMOVE_AUTHOR_BY_ID = "delete from AUTHORS where AUTH_ID=?";
	public static final String SQL_UPDATE_NEWS_AUTHOR = "update NEWS_AUTHORS set NA_AUTHOR_ID=? "
			+ "where NA_NEWS_ID=?";
	public static final String SQL_FETCH_NOT_EXPIRED_AUTHORS = "select AUTH_ID, AUTH_NAME from AUTHORS where AUTH_EXPIRED is null";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(Author author) throws DAOException {
		Long authorId;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_AUTHOR, new int[] { 1 })) {
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpired());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			authorId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorId;
	}

	@Override
	public void update(Author author) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_AUTHOR)) {
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpired());
			ps.setLong(3, author.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<Author> fetchAll() throws DAOException {
		List<Author> authorsList = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_ALL_AUTHORS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Author author = new Author(rs.getLong(1), rs.getString(2), rs.getTimestamp(3));
				authorsList.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorsList;
	}

	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_NEWS_AUTHOR)) {
			ps.setLong(1, authorId);
			ps.setLong(2, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void removeById(Long authorId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_AUTHOR_BY_ID)) {
			ps.setLong(1, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public Author fetchByName(String name) throws DAOException {
		Author author = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_AUTHOR_BY_NAME)) {
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(rs.getLong(1), name);
				author.setExpired(rs.getTimestamp(2));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	@Override
	public Author fetchByNewsId(Long newsId) throws DAOException {
		Author author = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_AUTHOR_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(rs.getLong(1), rs.getString(2), rs.getTimestamp(3));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	@Override
	public Author fetchById(Long authorId) throws DAOException {
		Author author = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_AUTHOR_BY_ID)) {
			ps.setLong(1, authorId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author(authorId, rs.getString(1), rs.getTimestamp(2));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	@Override
	public List<Author> fetchNotExpired() throws DAOException {
		List<Author> authorsList = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_NOT_EXPIRED_AUTHORS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Author author = new Author(rs.getLong(1), rs.getString(2));
				authorsList.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorsList;
	}

}
