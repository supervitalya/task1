package com.epam.task1.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.task1.dao.TagDAO;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;

@Component
public class TagDAOImpl implements TagDAO {

	public static final String SQL_ADD_TAG = "insert into TAGS(TAG_NAME) values(?)";
	public static final String SQL_REMOVE_TAG = "delete from TAGS where TAG_ID = ?";
	public static final String SQL_FETCH_TAG_BY_NAME = "select TAG_ID from TAGS where TAG_NAME=?";
	public static final String SQL_FETCH_TAG_BY_NEWS_ID = "select TAGS.TAG_ID, TAGS.TAG_NAME from "
			+ "NEWS_TAGS inner join TAGS on NEWS_TAGS.NT_TAG_ID=TAGS.TAG_ID where NEWS_TAGS.NT_NEWS_ID=?";
	public static final String SQL_FETCH_ALL_TAGS = "select TAG_ID, TAG_NAME from TAGS";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(Tag tag) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		Long tagId;
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_TAG, new int[] { 1 })) {
			ps.setString(1, tag.getTagName());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			tagId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Cannot add Tag entity " + tag, e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagId;
	}

	@Override
	public List<Tag> fetchAll() throws DAOException {
		List<Tag> tags = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_ALL_TAGS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Tag tag = new Tag(rs.getLong(1), rs.getString(2));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tags;
	}

	@Override
	public void removeById(Long tagId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_TAG)) {
			ps.setLong(1, tagId);
			ps.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/**
	 * The method returns tag by it's name. If there's no tag with such name
	 * returns null.
	 * 
	 * @throws DAOException
	 */

	@Override
	public Tag fetchByName(String tagName) throws DAOException {
		Tag tag = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_TAG_BY_NAME)) {
			ps.setString(1, tagName);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				tag = new Tag(rs.getLong(1), tagName);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tag;
	}

	@Override
	public List<Tag> fetchByNewsId(Long newsId) throws DAOException {
		List<Tag> tagList = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_TAG_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Tag tag = new Tag(rs.getLong(1), rs.getString(2));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagList;
	}

}
