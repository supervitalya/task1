package com.epam.task1.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.task1.dao.CommentDAO;
import com.epam.task1.domain.Comment;
import com.epam.task1.exception.DAOException;

@Component
public class CommentDAOImpl implements CommentDAO {

	public static final String SQL_REMOVE_COMMENT_BY_ID = "delete from COMMENTS where CMT_ID=?";
	public static final String SQL_REMOVE_COMMENT_BY_NEWS_ID = "delete from COMMENTS where CMT_NEWS_ID=?";
	public static final String SQL_ADD_COMMENT = "insert into COMMENTS (CMT_NEWS_ID,"
			+ " CMT_TEXT,CMT_CREATION_DATE) values(?,?,?)";

	public static final String SQL_SELECT_COMMENT_BY_NEWS_ID = "select COMMENTS.CMT_ID, COMMENTS.CMT_NEWS_ID,"
			+ "COMMENTS.CMT_TEXT, COMMENTS.CMT_CREATION_DATE from NEWS inner join COMMENTS on NEWS.NEWS_ID ="
			+ " COMMENTS.CMT_NEWS_ID where NEWS.NEWS_ID =?";

	public static final String SQL_UPDATE_COMMENT = "update COMMENTS set CMT_NEWS_ID=?, "
			+ "CMT_TEXT=?, CMT_CREATION_DATE=? where CMT_ID=?";

	@Autowired
	private DataSource dataSource;

	@Override
	public void removeById(Long commentId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_COMMENT_BY_ID)) {
			ps.setLong(1, commentId);
			ps.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public Long add(Comment comment) throws DAOException {
		Long commentId;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_COMMENT, new int[] { 1 })) {
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, comment.getCreationDate());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			commentId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Cannot add comment entity" + comment, e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return commentId;
	}

	@Override
	public void update(Comment comment) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_COMMENT)) {
			ps.setLong(4, comment.getId());
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, comment.getCreationDate());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void removeByNewsId(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_COMMENT_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ps.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<Comment> fetchByNewsId(Long newsId) throws DAOException {
		List<Comment> commentsList = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_SELECT_COMMENT_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Comment comment = new Comment(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getTimestamp(4));
				commentsList.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return commentsList;
	}

}
