package com.epam.task1.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.task1.dao.NewsDAO;
import com.epam.task1.dao.criteria.SearchCriteria;
import com.epam.task1.dao.criteria.SearchCriteriaQueryBuilder;
import com.epam.task1.domain.News;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;

@Component
public class NewsDAOImpl implements NewsDAO {

	public static final String SQL_ADD_NEWS = "insert into NEWS (NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE) values(?,?,?,?,?)";

	public static final String SQL_REMOVE_NEWS_BY_ID = "delete from NEWS where NEWS_ID = ?";

	public static final String SQL_FETCH_ALL_NEWS = "select NEWS_ID,NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE from NEWS order by NEWS_ID desc";

	public static final String SQL_FETCH_NEWS_BY_ID = "select NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE from NEWS where NEWS_ID=?";

	public static final String SQL_UPDATE_NEWS = "update NEWS set NEWS_TITLE=?,NEWS_SHORT_TEXT=?,"
			+ "NEWS_FULL_TEXT=?,NEWS_MODIFICATION_DATE=? where NEWS_ID=?";

	public static final String SQL_COUNT_NEWS = "select count(NEWS_ID) from NEWS";

	public static final String SQL_ADD_AUTHOR_TO_NEWS = "insert into NEWS_AUTHORS (NA_NEWS_ID,"
			+ "NA_AUTHOR_ID) values(?,?)";

	public static final String SQL_REMOVE_AUTHOR_BY_NEWS_ID = "delete from NEWS_AUTHORS where NA_NEWS_ID=?";

	public static final String SQL_ADD_TAG_TO_NEWS = "insert into NEWS_TAGS (NT_NEWS_ID,NT_TAG_ID) values(?,?)";

	public static final String SQL_REMOVE_TAGS_BY_NEWS_ID = "delete from NEWS_TAGS where NT_NEWS_ID  = ?";

	public static final String SQL_SORTED_BY_COMMENTS = "select NEWS_ID, NEWS_TITLE,"
			+ "NEWS_SHORT_TEXT, NEWS_FULL_TEXT,NEWS_CREATION_DATE,"
			+ "NEWS_MODIFICATION_DATE, count(*) from NEWS left join COMMENTS on NEWS_ID = CMT_NEWS_ID group by NEWS_ID, NEWS_TITLE,"
			+ "NEWS_SHORT_TEXT, NEWS_FULL_TEXT,NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE order by count(*) DESC";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(News news) throws DAOException {
		Long newsId;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_NEWS, new int[] { 1 })) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			newsId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsId;

	}

	@Override
	public void removeById(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_NEWS_BY_ID)) {
			ps.setLong(1, newsId);
			ps.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<News> fetchAll() throws DAOException {
		List<News> newsList = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_ALL_NEWS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getTimestamp(5));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}

	@Override
	public void update(News news) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_NEWS)) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setDate(4, news.getModificationDate());
			ps.setLong(5, news.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public long countNews() throws DAOException {
		long count = 0;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_COUNT_NEWS)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return count;
	}

	@Override
	public void addAuthorToNews(Long newsId, Long authorId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_AUTHOR_TO_NEWS)) {
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void removeAuthorRelationByNewsId(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_AUTHOR_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void addTagsToNews(Long newsId, List<Tag> tagsList) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_TAG_TO_NEWS)) {
			for (Tag tag : tagsList) {
				ps.setLong(1, newsId);
				ps.setLong(2, tag.getId());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void removeTagRelationsByNewsId(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_TAGS_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<News> fetchSortedByComments() throws DAOException {
		List<News> newsList = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_SORTED_BY_COMMENTS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getTimestamp(5));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}

	@Override
	public List<News> fetchByCriteria(SearchCriteria sc) throws DAOException {
		String searchQuery;
		List<News> newsList = new ArrayList<>();

		if ((sc.getAuthorsIdList() == null) || (sc.getAuthorsIdList().isEmpty())
				&& ((sc.getTagIdList() == null) || (sc.getTagIdList().isEmpty()))) {
			return newsList;
		}
		SearchCriteriaQueryBuilder builder = new SearchCriteriaQueryBuilder();
		searchQuery = builder.buildSearchQuery(sc);
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(searchQuery)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getTimestamp(5), rs.getDate(6));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}

	@Override
	public News fetchById(Long newsId) throws DAOException {
		News news = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_NEWS_BY_ID)) {
			ps.setLong(1, newsId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				news = new News(newsId, rs.getString(1), rs.getString(2), rs.getString(3), rs.getTimestamp(4),
						rs.getDate(5));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return news;
	}

}
