package com.epam.task1.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.task1.dao.UserDAO;
import com.epam.task1.domain.User;
import com.epam.task1.exception.DAOException;

@Component
public class UserDAOImpl implements UserDAO {

	public static final String SQL_ADD_USER = "insert into USERS (USR_NAME, USR_LOGIN,"
			+ " USR_PASSWORD) values(?,?,?)";

	public static final String SQL_REMOVE_USER = "delete from USERS where USR_ID=?";
	public static final String SQL_FETCH_USER_BY_LOGIN = "select USR_ID, USR_NAME, "
			+ "USR_PASSWORD from USERS where USR_LOGIN = ?";
	public static final String SQL_FETCH_USER_BY_ID = "select USR_NAME,USR_LOGIN, USR_PASSWORD"
			+ " from USERS where USR_ID = ?";
	public static final String SQL_UPDATE_USER = "update USERS set USR_NAME=?, USR_LOGIN=?,"
			+ "USR_PASSWORD=? where USR_ID=?";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(User user) throws DAOException {
		Long userId;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_USER, new int[] { 1 })) {
			ps.setString(1, user.getUserName());
			ps.setString(2, user.getLogin());
			ps.setString(3, user.getPassword());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			userId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException("Cannot add user " + user + ". ", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return userId;
	}

	@Override
	public void removeById(Long userId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_USER)) {
			ps.setLong(1, userId);
			ps.execute();
		} catch (SQLException e) {
			throw new DAOException("Cannot remove user id=" + userId, e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void update(User user) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_USER)) {
			ps.setString(1, user.getUserName());
			ps.setString(2, user.getLogin());
			ps.setString(3, user.getPassword());
			ps.setLong(4, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Cannot update user " + user + ". ", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public User fetchByLogin(String login) throws DAOException {
		User user = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_USER_BY_LOGIN)) {
			ps.setString(1, login);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user = new User(rs.getLong(1), rs.getString(2), login, rs.getString(3));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return user;
	}

	@Override
	public User fetchById(Long userId) throws DAOException {
		User user = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_USER_BY_ID)) {
			ps.setLong(1, userId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user = new User(userId, rs.getString(1), rs.getString(2), rs.getString(3));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return user;
	}

}
