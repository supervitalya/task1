package com.epam.task1.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.task1.dao.RoleDAO;
import com.epam.task1.domain.Role;
import com.epam.task1.exception.DAOException;

@Component
public class RoleDAOImpl implements RoleDAO {

	public static final String SQL_ROLE_ADD = "insert into ROLES (RL_USER_ID,RL_NAME) values(?,?)";
	public static final String SQL_ROLE_FETCH_BY_USER_ID = "select RL_ID,RL_ROLE_NAME from ROLES where RL_USER_ID=?";
	public static final String SQL_ROLE_REMOVE_BY_USER_ID = "delete from ROLES where RL_USER_ID = ?";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(Role role) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		Long roleId;
		try (PreparedStatement ps = con.prepareStatement(SQL_ROLE_ADD, new int[] { 1 })) {
			ps.setLong(1, role.getUserId());
			ps.setString(2, role.getRoleName());
			ps.executeQuery();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			roleId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return roleId;
	}

	@Override
	public List<Role> fetchByUserId(Long userId) throws DAOException {
		List<Role> roles = new ArrayList<>();
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ROLE_FETCH_BY_USER_ID)) {
			ps.setLong(1, userId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Role role = new Role(rs.getLong(1), userId, rs.getString(2));
				roles.add(role);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return roles;
	}

	@Override
	public void removeByUserId(Long userId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ROLE_REMOVE_BY_USER_ID)) {
			ps.setLong(1, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

}
