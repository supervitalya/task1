package com.epam.task1.dao.criteria;

import java.util.List;

/**
 * Utility class that provides method for generation query from SearchQuery
 * object.
 * 
 * @see SearchCriteria
 * @author Vitaly_Blyaharchuk
 *
 */
public class SearchCriteriaQueryBuilder {

	public static final String BASIC_QUERY_BY_TAGS = "NEWS_TAGS.NT_TAG_ID in(";
	public static final String BASIC_QUERY_BY_AUTHORS = "NEWS_AUTHORS.NA_AUTHOR_ID in(";

	public static final String BASIC_QUERY_BY_CRITERIA = "select distinct NEWS.NEWS_ID, NEWS.NEWS_TITLE,"
			+ "NEWS.NEWS_SHORT_TEXT, NEWS.NEWS_FULL_TEXT,NEWS.NEWS_CREATION_DATE,"
			+ "NEWS.NEWS_MODIFICATION_DATE from NEWS left join NEWS_AUTHORS on NEWS.NEWS_ID = "
			+ "NEWS_AUTHORS.NA_NEWS_ID left join NEWS_TAGS on NEWS.NEWS_ID = NEWS_TAGS.NT_NEWS_ID where ";

	/**
	 * Returns query built for searching News entities using lists of Tag and
	 * Author entities connected to id.
	 * 
	 * @param sc
	 *            search criteria object
	 * @return searching query
	 */

	public String buildSearchQuery(SearchCriteria sc) {
		StringBuilder query = new StringBuilder();
		List<Long> authorsIdList = sc.getAuthorsIdList();
		List<Long> tagsIdList = sc.getTagIdList();
		boolean authorAdded = false;

		query.append(BASIC_QUERY_BY_CRITERIA);

		if ((authorsIdList != null) && (!authorsIdList.isEmpty())) {
			query.append(buildAuthorsQuery(authorsIdList));
			authorAdded = true;
		}

		if ((tagsIdList != null) && (!tagsIdList.isEmpty())) {
			if (authorAdded) {
				query.append(" AND ");
			}
			query.append(buildTagsQuery(tagsIdList));
		}
		return query.toString();
	}

	private String buildTagsQuery(List<Long> tagsIdList) {
		StringBuilder query = new StringBuilder(BASIC_QUERY_BY_TAGS);
		for (Long tagId : tagsIdList) {
			query.append(tagId + ",");
		}
		query.deleteCharAt(query.length() - 1);
		query.append(")");
		return query.toString();
	}

	private String buildAuthorsQuery(List<Long> authorsIdList) {
		StringBuilder query = new StringBuilder(BASIC_QUERY_BY_AUTHORS);
		for (Long authorId : authorsIdList) {
			query.append(authorId + ",");
		}
		query.deleteCharAt(query.length() - 1);
		query.append(")");
		return query.toString();
	}
}
