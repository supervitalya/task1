package com.epam.task1.dao.criteria;

import java.util.List;

/**
 * Provides criteria for News entities searching. Contains list of Author
 * entities and list of Tag entities. Depending on lists searching will perform.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public class SearchCriteria {
	private List<Long> authorsIdList;
	private List<Long> tagsIdList;

	public SearchCriteria() {
		super();
	}

	public SearchCriteria(List<Long> authorsList, List<Long> tagList) {
		super();
		this.authorsIdList = authorsList;
		this.tagsIdList = tagList;
	}

	public List<Long> getAuthorsIdList() {
		return authorsIdList;
	}

	public void setAuthorsIdList(List<Long> authorsList) {
		this.authorsIdList = authorsList;
	}

	public List<Long> getTagIdList() {
		return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagList) {
		this.tagsIdList = tagList;
	}

}
