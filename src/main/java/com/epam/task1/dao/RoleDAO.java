package com.epam.task1.dao;

import java.util.List;

import com.epam.task1.domain.Role;
import com.epam.task1.exception.DAOException;

/**
 * Provides access to Role entities into database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface RoleDAO {
	/**
	 * Adds new role entity into database. Returns auto-generated id.
	 * 
	 * @param role
	 *            entity
	 * @return generated id
	 * @throws DAOException
	 */
	Long add(Role role) throws DAOException;

	/**
	 * Removes roles related to User entity with such id.
	 * 
	 * @param userId
	 *            User entity id
	 * @throws DAOException
	 */
	void removeByUserId(Long userId) throws DAOException;

	/**
	 * Fetches list of Role entities related to User entity with such id. If
	 * there's no such roles returns empty list.
	 * 
	 * @param userId
	 * @return
	 * @throws DAOException
	 */
	List<Role> fetchByUserId(Long userId) throws DAOException;
}
