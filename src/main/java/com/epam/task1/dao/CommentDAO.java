package com.epam.task1.dao;

import java.util.List;

import com.epam.task1.domain.Comment;
import com.epam.task1.exception.DAOException;

/**
 * Provides access to Comment entities into database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface CommentDAO {
	/**
	 * Removes comment entity with such id from database.
	 * 
	 * @param commentId
	 *            comment entity id
	 * @throws DAOException
	 */

	void removeById(Long commentId) throws DAOException;

	/**
	 * Adds comment entity into database.
	 * 
	 * @param comment
	 *            entity
	 * @throws DAOException
	 */

	Long add(Comment comment) throws DAOException;

	/**
	 * Updates comment.
	 * 
	 * @param comment
	 *            entity
	 * @throws DAOException
	 */

	void update(Comment comment) throws DAOException;

	/**
	 * Removes all comments related to News entity with such id.
	 * 
	 * @param newsId
	 *            news entity id
	 * @throws DAOException
	 */
	void removeByNewsId(Long newsId) throws DAOException;

	/**
	 * Fetches all comments which are connected to news entity with such id. If
	 * there's no comments related to the News entity returns empty list.
	 * 
	 * @param newsId
	 *            news entity id
	 * @return list of news comments
	 * @throws DAOException
	 */
	List<Comment> fetchByNewsId(Long newsId) throws DAOException;
}
