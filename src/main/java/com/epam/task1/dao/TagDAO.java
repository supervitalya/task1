package com.epam.task1.dao;

import java.util.List;

import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;

/**
 * Provides access to Tag entities into database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface TagDAO {
	/**
	 * Adds new tag into database. Returns it's auto-generated id.
	 * 
	 * @param tag
	 *            entity
	 * @return tag's id
	 * @throws DAOException
	 */
	Long add(Tag tag) throws DAOException;

	/**
	 * Fetches list of all tags. If there's no tags into database returns empty
	 * list.
	 * 
	 * @return List of tags
	 * @throws DAOException
	 */
	List<Tag> fetchAll() throws DAOException;

	/**
	 * Removes Tag entity with such id.
	 * 
	 * @param tagId
	 *            tag entity id
	 * @throws DAOException
	 */
	void removeById(Long tagId) throws DAOException;

	/**
	 * Fetches Tag entity by it's name. If there's no such entity returns null.
	 * 
	 * @param tagName
	 *            tag entity name
	 * @return tag entity
	 * @throws DAOException
	 */
	Tag fetchByName(String tagName) throws DAOException;

	/**
	 * Fetches tag entities related to news entity with such id. If there's no
	 * such tags returns empty list.
	 * 
	 * @param newsId
	 *            news entity id
	 * @return list of tags
	 * @throws DAOException
	 */
	List<Tag> fetchByNewsId(Long newsId) throws DAOException;
}
