package com.epam.task1.dao;

import java.util.List;

import com.epam.task1.domain.Author;
import com.epam.task1.exception.DAOException;

/**
 * Provides access to Author entities in database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface AuthorDAO {
	/**
	 * Adds Author entity into database. Returns generated key.
	 * 
	 * @return auto-generated key.
	 * @throws DAOException
	 */
	Long add(Author author) throws DAOException;

	/**
	 * Updates Author entity into database.
	 * 
	 * @return List of Author entities.
	 * @throws DAOException
	 */
	void update(Author author) throws DAOException;

	/**
	 * Fetches all Author entities from database. Returns empty list if there's
	 * no entities in Authors table.
	 * 
	 * @return List of Author entities.
	 * @throws DAOException
	 */
	List<Author> fetchAll() throws DAOException;

	/**
	 * Removes Author entity with such id.
	 * 
	 * @param authorId
	 *            author entity id
	 * @throws DAOException
	 */
	void removeById(Long authorId) throws DAOException;

	/**
	 * Updates connection between News and Author.
	 * 
	 * @param newsId
	 *            news entity id.
	 * @param authorId
	 *            author entity id
	 * @throws DAOException
	 */
	void updateNewsAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Fetches Author entity with defined name or returns null when there's no
	 * entity with such name.
	 * 
	 * @param name
	 *            author entity name
	 * @return author entity with such name
	 * @throws DAOException
	 */
	Author fetchByName(String name) throws DAOException;

	/**
	 * Fetches Author of news entity with such id or returns null when there's
	 * no news entity with such id.
	 * 
	 * @param newsId
	 *            author entity id
	 * @return author entity with such id
	 * @throws DAOException
	 */
	Author fetchByNewsId(Long newsId) throws DAOException;

	/**
	 * Fetches Author entity with such id or returns null when there's no such
	 * entity.
	 * 
	 * @param authorId
	 * @return
	 * @throws DAOException
	 */
	Author fetchById(Long authorId) throws DAOException;

	/**
	 * Fetches all Author entities which are not expired. Returns empty list if
	 * there's no such entities.
	 * 
	 * @return List of not expired Author entities.
	 * @throws DAOException
	 */
	List<Author> fetchNotExpired() throws DAOException;

}
