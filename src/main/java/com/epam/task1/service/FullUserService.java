package com.epam.task1.service;

import com.epam.task1.domain.UserDTO;
import com.epam.task1.exception.ServiceException;

public interface FullUserService {

	/**
	 * Fetches user transfer object which contains User entity and list of roles
	 * connected to it.
	 * 
	 * @param login
	 *            User entity login
	 * @return user transfer object
	 * @throws ServiceException
	 */
	UserDTO openUserByLogin(String login) throws ServiceException;

	/**
	 * Fetches user transfer object which contains User entity and list of roles
	 * connected to it.
	 * 
	 * @param userId
	 *            User entity id
	 * @return user transfer object
	 * @throws ServiceException
	 */
	UserDTO openUserById(Long userId) throws ServiceException;

	/**
	 * Removes User entity and all Roles entities connected to it
	 * 
	 * @param userId
	 *            User entity id
	 * @throws ServiceException
	 */
	void removeFullUser(Long userId) throws ServiceException;

}
