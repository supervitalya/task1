package com.epam.task1.service;

import java.util.List;

import com.epam.task1.domain.NewsDTO;
import com.epam.task1.exception.ServiceException;

public interface FullNewsService {

	NewsDTO openNews(Long newsId) throws ServiceException;

	List<NewsDTO> openNewsTOList() throws ServiceException;

	void removeFullNews(Long newsId) throws ServiceException;

}
