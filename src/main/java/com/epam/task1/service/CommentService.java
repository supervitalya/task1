package com.epam.task1.service;

import java.util.List;

import com.epam.task1.domain.Comment;
import com.epam.task1.exception.ServiceException;

/**
 * Provides methods for actions with Comment entities.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface CommentService {
	/**
	 * Adds Comment entity.
	 * 
	 * @param comment
	 *            entity
	 * @throws ServiceException
	 */
	Comment addComment(Comment comment) throws ServiceException;

	/**
	 * Removes Comment entity with specified id.
	 * 
	 * @param commentId
	 *            comment entity id
	 * @throws ServiceException
	 */
	void removeComment(Long commentId) throws ServiceException;

	/**
	 * Removes all comments related to news entity with specified id.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	void removeAllComments(Long newsId) throws ServiceException;

	/**
	 * Returns list of comments attached to News entity with specified id.
	 * 
	 * @param newsId
	 *            news entity id
	 * @return list of news comments
	 * @throws ServiceException
	 */
	List<Comment> fetchByNewsId(Long newsId) throws ServiceException;
}
