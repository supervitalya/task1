package com.epam.task1.service;

import com.epam.task1.domain.User;
import com.epam.task1.exception.ServiceException;

/**
 * Provides method for actions with User entities.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface UserService {
	/**
	 * Adds User entity.
	 * 
	 * @param user
	 *            entity
	 * @return true if adding is successful. If something goes wrong or User
	 *         with such login is already exits returns false.
	 * @throws ServiceException
	 */
	User addUser(User user) throws ServiceException;

	/**
	 * Updates User entity.
	 * 
	 * @param user
	 *            entity
	 * @throws ServiceException
	 */
	void updateUser(User user) throws ServiceException;

	/**
	 * Removes User entity with specified id.
	 * 
	 * @param userId
	 *            User entity id
	 * @throws ServiceException
	 */
	void removeUser(Long userId) throws ServiceException;

	/**
	 * Fetches User entity with specified id.
	 * 
	 * @param userId
	 *            User entity id
	 * @return user entity
	 * @throws ServiceException
	 */
	User fetchUserById(Long userId) throws ServiceException;

	/**
	 * Fetches User entity with specified login
	 * 
	 * @param login
	 *            User entity login
	 * @return User entity
	 */
	User fetchUserByLogin(String login) throws ServiceException;

}
