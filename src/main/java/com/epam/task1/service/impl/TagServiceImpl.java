package com.epam.task1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.task1.dao.TagDAO;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.IllegalEntityException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.TagService;

@Service("tagService")
public class TagServiceImpl implements TagService {
	@Autowired
	private TagDAO tagDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Tag addTag(Tag tag) throws ServiceException {
		try {
			String tagName = tag.getTagName();
			if (tagDao.fetchByName(tagName) != null) {
				throw new IllegalEntityException("Tag with specified name already exist");
			}
			Long tagId = tagDao.add(tag);
			tag.setId(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return tag;
	}

	@Override
	public void removeTag(Long tagId) throws ServiceException {
		try {
			tagDao.removeById(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> fetchAllTags() throws ServiceException {
		try {
			return tagDao.fetchAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> fetchByNewsId(Long newsId) throws ServiceException {
		try {
			return tagDao.fetchByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
