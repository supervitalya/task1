package com.epam.task1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.task1.dao.CommentDAO;
import com.epam.task1.domain.Comment;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.CommentService;

@Service("commentService")
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDAO commentDao;

	@Override
	public Comment addComment(Comment comment) throws ServiceException {
		try {
			Long commentId = commentDao.add(comment);
			comment.setId(commentId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return comment;
	}

	@Override
	public void removeComment(Long commentId) throws ServiceException {
		try {
			commentDao.removeById(commentId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeAllComments(Long newsId) throws ServiceException {
		try {
			commentDao.removeByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Comment> fetchByNewsId(Long newsId) throws ServiceException {
		try {
			return commentDao.fetchByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
