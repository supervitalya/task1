package com.epam.task1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.task1.dao.UserDAO;
import com.epam.task1.domain.User;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.IllegalEntityException;
import com.epam.task1.exception.NoSuchEntityException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO userDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public User addUser(User user) throws ServiceException {
		try {
			String userLogin = user.getLogin();
			if (userDao.fetchByLogin(userLogin) != null) {
				throw new IllegalEntityException("User with specified login already exist");
			}
			Long userId = userDao.add(user);
			user.setId(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return user;
	}

	@Override
	public void updateUser(User user) throws ServiceException {
		try {
			userDao.update(user);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeUser(Long userId) throws ServiceException {
		try {
			userDao.removeById(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public User fetchUserById(Long userId) throws ServiceException {
		User user = null;
		try {
			user = userDao.fetchById(userId);
			if (user == null) {
				throw new NoSuchEntityException("User not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return user;
	}

	@Override
	public User fetchUserByLogin(String login) throws ServiceException {
		User user = null;
		try {
			user = userDao.fetchByLogin(login);
			if (user == null) {
				throw new NoSuchEntityException("User not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return user;
	}
}
