package com.epam.task1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.task1.domain.Role;
import com.epam.task1.domain.User;
import com.epam.task1.domain.UserDTO;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.FullUserService;
import com.epam.task1.service.RoleService;
import com.epam.task1.service.UserService;

@Service("fullUserService")
public class FullUserServiceImpl implements FullUserService {

	@Autowired
	UserService userService;

	@Autowired
	RoleService roleService;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public UserDTO openUserByLogin(String login) throws ServiceException {
		UserDTO uto = new UserDTO();
		User user = userService.fetchUserByLogin(login);
		if (user == null) {
			return null;
		}
		List<Role> roles = roleService.fetchByUserId(user.getId());
		uto.setUser(user);
		uto.setRolesList(roles);
		return uto;
	}

	@Override
	public UserDTO openUserById(Long userId) throws ServiceException {
		UserDTO uto = new UserDTO();
		User user = userService.fetchUserById(userId);
		if (user == null) {
			return null;
		}
		List<Role> roles = roleService.fetchByUserId(user.getId());
		uto.setUser(user);
		uto.setRolesList(roles);
		return uto;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void removeFullUser(Long userId) throws ServiceException {
		userService.removeUser(userId);
		roleService.removeRoleByUserId(userId);
	}

}
