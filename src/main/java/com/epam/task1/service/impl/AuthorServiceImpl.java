package com.epam.task1.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.task1.dao.AuthorDAO;
import com.epam.task1.domain.Author;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.IllegalEntityException;
import com.epam.task1.exception.NoSuchEntityException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.AuthorService;

@Service("authorService")
public class AuthorServiceImpl implements AuthorService {
	@Autowired
	private AuthorDAO authorDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Author addAuthor(Author author) throws ServiceException {
		try {
			if (authorDao.fetchByName(author.getAuthorName()) != null) {
				throw new IllegalEntityException("Author with such name already exist.");
			}
			Long authorId = authorDao.add(author);
			author.setId(authorId);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void setAuthorExpired(Long authorId) throws ServiceException {
		try {
			Author newAuthor = authorDao.fetchById(authorId);
			if (newAuthor == null) {
				throw new NoSuchEntityException("Author not found");
			}
			newAuthor.setExpired(new Timestamp(System.currentTimeMillis()));
			authorDao.update(newAuthor);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Author fetchAuthor(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.fetchById(authorId);
			if (author == null) {
				throw new NoSuchEntityException("Author not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	@Override
	public List<Author> fetchAllAuthors() throws ServiceException {
		List<Author> authorsList = new ArrayList<>();
		try {
			authorsList = authorDao.fetchAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return authorsList;
	}

	@Override
	public List<Author> fetchNotExpiredAuthors() throws ServiceException {
		List<Author> authorsList = new ArrayList<>();
		try {
			authorsList = authorDao.fetchNotExpired();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return authorsList;
	}

	@Override
	public Author fetchByNewsId(Long newsId) throws ServiceException {
		Author author;
		try {
			author = authorDao.fetchByNewsId(newsId);
			if (author == null) {
				throw new NoSuchEntityException("Author not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}
}
