package com.epam.task1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.task1.dao.RoleDAO;
import com.epam.task1.domain.Role;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.RoleService;

@Service("roleService")
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDAO roleDao;

	@Override
	public Role addRole(Role role) throws ServiceException {
		try {
			Long roleId = roleDao.add(role);
			role.setId(roleId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return role;
	}

	@Override
	public void removeRoleByUserId(Long userId) throws ServiceException {
		try {
			roleDao.removeByUserId(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Role> fetchByUserId(Long userId) throws ServiceException {
		try {
			return roleDao.fetchByUserId(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
