package com.epam.task1.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.task1.domain.News;
import com.epam.task1.domain.NewsDTO;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.AuthorService;
import com.epam.task1.service.CommentService;
import com.epam.task1.service.FullNewsService;
import com.epam.task1.service.NewsService;
import com.epam.task1.service.TagService;

@Service("fullNewsService")
public class FullNewsServiceImpl implements FullNewsService {
	@Autowired
	TagService tagService;
	@Autowired
	NewsService newsService;
	@Autowired
	AuthorService authorService;
	@Autowired
	CommentService commentService;

	@Override
	public NewsDTO openNews(Long newsId) throws ServiceException {
		NewsDTO newsTO = new NewsDTO();
		News news = newsService.fetchNews(newsId);
		if (news == null) {
			return null;
		}
		newsTO.setNews(news);
		newsTO.setTagsList(tagService.fetchByNewsId(newsId));
		newsTO.setAuthor(authorService.fetchByNewsId(newsId));
		newsTO.setCommentsList(commentService.fetchByNewsId(newsId));
		return newsTO;
	}

	@Override
	public List<NewsDTO> openNewsTOList() throws ServiceException {
		List<NewsDTO> transferObjectsList = new ArrayList<>();
		List<News> newsList = newsService.fetchNewsList();
		for (News n : newsList) {
			long newsId = n.getId();
			NewsDTO nto = new NewsDTO();
			nto.setNews(n);
			nto.setTagsList(tagService.fetchByNewsId(newsId));
			nto.setAuthor(authorService.fetchByNewsId(newsId));
			nto.setCommentsList(commentService.fetchByNewsId(newsId));
			transferObjectsList.add(nto);
		}
		return transferObjectsList;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void removeFullNews(Long newsId) throws ServiceException {
		newsService.removeNews(newsId);
		commentService.removeAllComments(newsId);
		newsService.removeAllTagsFromNews(newsId);
		newsService.removeAuthorFromNews(newsId);
	}

}
