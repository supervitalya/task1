package com.epam.task1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.task1.dao.NewsDAO;
import com.epam.task1.dao.criteria.SearchCriteria;
import com.epam.task1.domain.News;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.NoSuchEntityException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.NewsService;

@Service("newsService")
public class NewsServiceImpl implements NewsService {
	@Autowired
	private NewsDAO newsDao;

	@Override
	public News addNews(News news) throws ServiceException {
		try {
			Long newsId = newsDao.add(news);
			news.setId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	@Override
	public List<News> fetchNewsList() throws ServiceException {
		try {
			return newsDao.fetchAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeNews(Long newsId) throws ServiceException {
		try {
			newsDao.removeById(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void editNews(News news) throws ServiceException {
		try {
			newsDao.update(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> searchByCriteria(SearchCriteria sc) throws ServiceException {
		try {
			return newsDao.fetchByCriteria(sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public long countNews() throws ServiceException {
		try {
			return newsDao.countNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> fetchNewsSortedByComments() throws ServiceException {
		try {
			return newsDao.fetchSortedByComments();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public News fetchNews(Long newsId) throws ServiceException {
		News news = null;
		try {
			news = newsDao.fetchById(newsId);
			if (news == null) {
				throw new NoSuchEntityException("News not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDao.removeAuthorRelationByNewsId(newsId);
			newsDao.addAuthorToNews(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeAllTagsFromNews(Long newsId) throws ServiceException {
		try {
			newsDao.removeTagRelationsByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addTagsToNews(Long newsId, List<Tag> tagsList) throws ServiceException {
		try {
			newsDao.addTagsToNews(newsId, tagsList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeAuthorFromNews(Long newsId) throws ServiceException {
		try {
			newsDao.removeAuthorRelationByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
