package com.epam.task1.service;

import java.util.List;

import com.epam.task1.domain.Role;
import com.epam.task1.exception.ServiceException;

/**
 * Provides methods for actions with Role entities.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface RoleService {
	/**
	 * Adds new role entity.
	 * 
	 * @param role
	 *            entity
	 * @throws ServiceException
	 */
	Role addRole(Role role) throws ServiceException;

	/**
	 * Removes Role entity with specified id.
	 * 
	 * @param userId
	 *            Role entity id.
	 * @throws ServiceException
	 */
	void removeRoleByUserId(Long userId) throws ServiceException;

	/**
	 * Fetches Role entities list connected to user with specified id.
	 * 
	 * @param userId
	 *            User entity id
	 * @return list of Role entities
	 * @throws ServiceException
	 */
	List<Role> fetchByUserId(Long userId) throws ServiceException;
}
