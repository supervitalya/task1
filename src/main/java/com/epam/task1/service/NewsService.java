package com.epam.task1.service;

import java.util.List;

import com.epam.task1.dao.criteria.SearchCriteria;
import com.epam.task1.domain.News;
import com.epam.task1.domain.NewsDTO;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.ServiceException;

public interface NewsService {
	/**
	 * Adds News entity which.
	 * 
	 * @param news
	 *            transfer object
	 * @throws ServiceException
	 */
	News addNews(News news) throws ServiceException;

	/**
	 * Updates News entity.
	 * 
	 * @param news
	 *            entity
	 * @throws ServiceException
	 */
	void editNews(News news) throws ServiceException;

	/**
	 * Removes News entity with specified id.
	 * 
	 * @param newsId
	 *            news entity id.
	 * @throws ServiceException
	 */
	void removeNews(Long newsId) throws ServiceException;

	/**
	 * Searches list of news according to SearchCriteria object. If there's no
	 * such News entity objects returns empty list.
	 * 
	 * @see SearchCriteria
	 * @param sc
	 *            search criteria object
	 * @return list of News entities
	 * @throws ServiceException
	 */
	List<News> searchByCriteria(SearchCriteria sc) throws ServiceException;

	/**
	 * Counts all News entities.
	 * 
	 * @return number of news entities
	 * @throws ServiceException
	 */
	long countNews() throws ServiceException;

	/**
	 * Fetches list of News entities sorted by amount of comments descending.
	 * 
	 * @return list of news entities
	 * @throws ServiceException
	 */
	List<News> fetchNewsSortedByComments() throws ServiceException;

	/**
	 * Returns News transfer object which contains News entity, list of tags,
	 * list of comments and Author entity related to it.
	 * 
	 * @see NewsDTO
	 * @param newsId
	 *            News entity id
	 * @return news transfer object
	 * @throws ServiceException
	 */
	News fetchNews(Long newsId) throws ServiceException;

	/**
	 * Fetches list of News entities.
	 * 
	 * @return list of News entities
	 * @throws ServiceException
	 */
	List<News> fetchNewsList() throws ServiceException;

	/**
	 * Updates connection between News entity with specified id and Author
	 * entity with specified id.
	 * 
	 * @param newsId
	 *            news entity id
	 * @param authorId
	 *            author entity id
	 * @throws ServiceException
	 */
	void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Adds connections between specified list of tags and News entity with
	 * specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @param tagsList
	 *            list of Tag entities
	 * @throws ServiceException
	 */
	void addTagsToNews(Long newsId, List<Tag> tagsList) throws ServiceException;

	/**
	 * Remove connections between all tags and News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @throws ServiceException
	 */
	void removeAllTagsFromNews(Long newsId) throws ServiceException;

	/**
	 * Remove connections between Author entity and News entity with specified
	 * id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @throws ServiceException
	 */
	void removeAuthorFromNews(Long newsId) throws ServiceException;

}
