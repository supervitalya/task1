package com.epam.task1.service;

import java.util.List;

import com.epam.task1.domain.Tag;
import com.epam.task1.exception.ServiceException;

/**
 * Provides methods for actions with Tag entities.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface TagService {
	/**
	 * Adds Tag entity.
	 * 
	 * @param tag
	 *            entity
	 * @return true if adding is successful. If something goes wrong or tag with
	 *         such name is already exist returns false.
	 * @throws ServiceException
	 */
	Tag addTag(Tag tag) throws ServiceException;

	/**
	 * Removes Tag entity.
	 * 
	 * @param tagId
	 *            Tag entity id
	 * @throws ServiceException
	 */
	void removeTag(Long tagId) throws ServiceException;

	/**
	 * Fetches list of all stored tags.
	 * 
	 * @return list of Tag entities
	 * @throws ServiceException
	 */
	List<Tag> fetchAllTags() throws ServiceException;

	/**
	 * Fetches list of all News tags.
	 * 
	 * @return list of news tags
	 * @throws ServiceException
	 */

	List<Tag> fetchByNewsId(Long newsId) throws ServiceException;

}
