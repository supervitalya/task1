package com.epam.task1.domain;

import java.sql.Date;
import java.sql.Timestamp;

public class News {
	private Long id;
	private String title;
	private String shortText;
	private String fullText;
	private Timestamp creationDate;
	private Date modificationDate;

	public News() {
		super();
	}

	public News(String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
		super();
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(Long id, String title, String shortText, String fullText, Timestamp creationDate) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
	}

	public News(Long id, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/**
	 * @Override public boolean equals(Object obj) { if (this == obj) return
	 *           true; if (obj == null) return false; if (getClass() !=
	 *           obj.getClass()) return false; News other = (News) obj; if
	 *           (creationDate == null) { if (other.creationDate != null) return
	 *           false; } else if (!creationDate.equals(other.creationDate))
	 *           return false; if (fullText == null) { if (other.fullText !=
	 *           null) return false; } else if
	 *           (!fullText.equals(other.fullText)) return false; if (id ==
	 *           null) { if (other.id != null) return false; } else if
	 *           (!id.equals(other.id)) return false; if (modificationDate ==
	 *           null) { if (other.modificationDate != null) return false; }
	 *           else if (!modificationDate.equals(other.modificationDate))
	 *           return false; if (shortText == null) { if (other.shortText !=
	 *           null) return false; } else if
	 *           (!shortText.equals(other.shortText)) return false; if (title ==
	 *           null) { if (other.title != null) return false; } else if
	 *           (!title.equals(other.title)) return false; return true; }
	 **/

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
