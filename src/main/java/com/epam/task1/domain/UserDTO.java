package com.epam.task1.domain;

import java.util.List;
/**
 * Entity contains User entity and list of Role entities.
 * Used to transfer data to view layer.
 */
public class UserDTO {
	private User user;
	private List<Role> rolesList;

	public UserDTO() {
		super();
	}

	public UserDTO(User user, List<Role> rolesList) {
		super();
		this.user = user;
		this.rolesList = rolesList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Role> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<Role> rolesList) {
		this.rolesList = rolesList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rolesList == null) ? 0 : rolesList.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		if (rolesList == null) {
			if (other.rolesList != null)
				return false;
		} else if (!rolesList.equals(other.rolesList))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserTO [user=" + user + ", rolesList=" + rolesList + "]";
	}

}
