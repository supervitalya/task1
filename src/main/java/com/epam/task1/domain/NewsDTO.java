package com.epam.task1.domain;

import java.util.List;
/**
 * Entity contains News entity, Author entity, list of Tag entities, and list of Comment entities.
 * Used to transfer data to view layer.
 *
 */
 
public class NewsDTO {
	private News news;
	private Author author;
	private List<Tag> tagsList;
	private List<Comment> commentsList;

	public NewsDTO() {
	}

	public NewsDTO(News news, Author author, List<Tag> tagsList, List<Comment> commentsList) {
		this.news = news;
		this.author = author;
		this.tagsList = tagsList;
		this.commentsList = commentsList;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTagsList() {
		return tagsList;
	}

	public void setTagsList(List<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public List<Comment> getCommentsList() {
		return commentsList;
	}

	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((commentsList == null) ? 0 : commentsList.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tagsList == null) ? 0 : tagsList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsDTO other = (NewsDTO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (commentsList == null) {
			if (other.commentsList != null)
				return false;
		} else if (!commentsList.equals(other.commentsList))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tagsList == null) {
			if (other.tagsList != null)
				return false;
		} else if (!tagsList.equals(other.tagsList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsTO [news=" + news + ", author=" + author + ", tagsList=" + tagsList + ", commentsList="
				+ commentsList + "]";
	}

}
