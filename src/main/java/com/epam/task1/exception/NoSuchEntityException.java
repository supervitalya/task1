package com.epam.task1.exception;

@SuppressWarnings("serial")
public class NoSuchEntityException extends RuntimeException {

	public NoSuchEntityException() {
		super();
	}

	public NoSuchEntityException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public NoSuchEntityException(String s, Throwable t) {
		super(s, t);
	}

	public NoSuchEntityException(String s) {
		super(s);
	}

	public NoSuchEntityException(Throwable t) {
		super(t);
	}

}
