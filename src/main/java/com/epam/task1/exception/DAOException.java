package com.epam.task1.exception;

@SuppressWarnings("serial")
public class DAOException extends Exception {

	public DAOException() {
		super();
	}

	public DAOException(String s, Throwable t, boolean arg2, boolean arg3) {
		super(s, t, arg2, arg3);
	}

	public DAOException(String s, Throwable t) {
		super(s, t);
	}

	public DAOException(String s) {
		super(s);
	}

	public DAOException(Throwable t) {
		super(t);
	}

}
