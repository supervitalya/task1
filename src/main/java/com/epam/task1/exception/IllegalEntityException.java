package com.epam.task1.exception;

@SuppressWarnings("serial")
public class IllegalEntityException extends RuntimeException {

	public IllegalEntityException() {
		super();
	}

	public IllegalEntityException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public IllegalEntityException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public IllegalEntityException(String arg0) {
		super(arg0);
	}

	public IllegalEntityException(Throwable arg0) {
		super(arg0);
	}

}
