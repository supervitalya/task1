package com.epam.task1.service;

import static org.mockito.Mockito.times;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.dao.AuthorDAO;
import com.epam.task1.dao.CommentDAO;
import com.epam.task1.dao.NewsDAO;
import com.epam.task1.dao.TagDAO;
import com.epam.task1.domain.News;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.impl.NewsServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)

public class NewsServiceImplTest {
	@Mock
	private AuthorDAO authorDao;
	@Mock
	private NewsDAO newsDao;
	@Mock
	private CommentDAO commentsDao;
	@Mock
	private TagDAO tagDao;
	@InjectMocks
	private NewsServiceImpl service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	/*
	 * @Test public void addNewsTest() throws ServiceException, DAOException {
	 * News news = new News(1L, "Title", "Shorttext", "longtext", new
	 * Timestamp(System.currentTimeMillis()), new
	 * Date(System.currentTimeMillis()));
	 * Mockito.when(newsDao.add(news)).thenReturn(1L); Author author = new
	 * Author(2L, "Ringo Star"); List<Tag> tagList = new ArrayList<>(); NewsTO
	 * nto = new NewsTO(); nto.setAuthor(author); nto.setNews(news);
	 * nto.setTagsList(tagList); service.addNews(news); Mockito.verify(newsDao,
	 * times(1)).add(news); Mockito.verify(newsDao,
	 * times(1)).addAuthorToNews(nto.getNews().getId(), author.getId());
	 * Mockito.verify(newsDao, times(1)).addTagsToNews(news.getId(),
	 * nto.getTagsList()); }
	 */

	@Test
	public void fetchNewsListTest() throws ServiceException, DAOException {
		long ct = System.currentTimeMillis();
		List<News> list = new ArrayList<>();
		list.add(new News(1L, "Title", "Shorttext", "longtext", new Timestamp(ct), new Date(ct)));
		Mockito.when(newsDao.fetchAll()).thenReturn(list);
		service.fetchNewsList();
		Mockito.verify(newsDao, times(1)).fetchAll();

	}

	@Test
	public void removeNewsTest() throws ServiceException, DAOException {
		long newsId = 1L;
		service.removeNews(newsId);
		Mockito.verify(newsDao, times(1)).removeById(newsId);
	}

	@Test
	public void updateNewsAuthorTest() throws ServiceException, DAOException {
		long newsId = 1L;
		long authorId = 2L;
		service.updateNewsAuthor(newsId, authorId);
		Mockito.verify(newsDao, times(1)).removeAuthorRelationByNewsId(newsId);
		Mockito.verify(newsDao, times(1)).addAuthorToNews(newsId, authorId);
	}
}
