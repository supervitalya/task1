package com.epam.task1.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.dao.AuthorDAO;
import com.epam.task1.dao.NewsDAO;
import com.epam.task1.domain.Author;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.impl.AuthorServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)

public class AuthorServiceImplTest {
	@Mock
	private AuthorDAO authorDao;
	@Mock
	private NewsDAO newsDao;

	@InjectMocks
	private AuthorServiceImpl service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addAuthorTest() throws ServiceException, DAOException {
		String name = "John Lennon";
		Mockito.when(authorDao.fetchByName(name)).thenReturn(null);
		Author author = new Author(1L, name);
		service.addAuthor(author);
		Mockito.verify(authorDao, times(1)).fetchByName(name);
		Mockito.verify(authorDao, times(1)).add(author);
	}

	@Test
	public void setAuthorExpiredTest() throws ServiceException, DAOException {
		long authorId = 1L;
		Author author = new Author(authorId, "Paul McCartney");
		Mockito.when(authorDao.fetchById(authorId)).thenReturn(author);
		service.setAuthorExpired(authorId);
		Mockito.verify(authorDao, times(1)).update(author);
		assertNotNull(author.getExpired());
	}

	@Test
	public void fetchAuthor() throws ServiceException, DAOException {
		long authorId = 1L;
		Author expected = new Author(authorId, "Evgeniy Petrosyan");
		Mockito.when(authorDao.fetchById(authorId)).thenReturn(expected);
		Author actual = service.fetchAuthor(authorId);
		Mockito.verify(authorDao, times(1)).fetchById(authorId);
		Assert.assertEquals(expected, actual);
	}

}
