package com.epam.task1.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.domain.Role;
import com.epam.task1.domain.User;
import com.epam.task1.domain.UserDTO;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.impl.FullUserServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)

public class FullUserServiceTest {
	@Mock
	private UserService userService;
	@Mock
	private RoleService roleService;

	@InjectMocks
	private FullUserServiceImpl fullUserService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void openUserByLoginTest() throws ServiceException {
		String login = "login";
		long userId = 1L;
		User user = new User(userId, login, "asd", "asd");
		List<Role> roles = new ArrayList<>();
		roles.add(new Role(1L, "admin"));
		roles.add(new Role(2L, "user"));
		UserDTO utoBefore = new UserDTO();
		utoBefore.setUser(user);
		utoBefore.setRolesList(roles);
		Mockito.when(userService.fetchUserByLogin(login)).thenReturn(user);
		Mockito.when(roleService.fetchByUserId(userId)).thenReturn(roles);
		UserDTO utoAfter = fullUserService.openUserByLogin(login);
		assertEquals(utoBefore, utoAfter);
	}

	@Test
	public void removeFullUserTest() throws ServiceException {
		long userId = 1L;
		fullUserService.removeFullUser(userId);
		Mockito.verify(userService, times(1)).removeUser(userId);
		Mockito.verify(roleService, times(1)).removeRoleByUserId(userId);
	}
}
