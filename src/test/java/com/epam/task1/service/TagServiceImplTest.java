package com.epam.task1.service;

import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.dao.NewsDAO;
import com.epam.task1.dao.TagDAO;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.impl.TagServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceImplTest {
	@Mock
	private TagDAO tagDao;
	@Mock
	private NewsDAO newsDao;
	@InjectMocks
	private TagServiceImpl service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addTagTest() throws DAOException, ServiceException {
		Tag tag = new Tag(1L, "Tag");
		service.addTag(tag);
		Mockito.verify(tagDao, times(1)).fetchByName(tag.getTagName());
		Mockito.verify(tagDao, times(1)).add(tag);
	}

	@Test
	public void removeTagTest() throws DAOException, ServiceException {
		long tagId = 1L;
		service.removeTag(tagId);
		Mockito.verify(tagDao, times(1)).removeById(tagId);
	}

}
