package com.epam.task1.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.dao.RoleDAO;
import com.epam.task1.dao.UserDAO;
import com.epam.task1.domain.User;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.impl.UserServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceImplTest {

	@Mock
	private RoleDAO roleDao;
	@Mock
	private UserDAO userDao;
	@InjectMocks
	private UserServiceImpl service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addUserTest() throws ServiceException, DAOException {
		User user = new User(22L, "Jimmy Hendrix", "Jimmy", "pass");
		service.addUser(user);
		Mockito.verify(userDao, times(1)).fetchByLogin(user.getLogin());
		Mockito.verify(userDao, times(1)).add(user);
	}

	@Test
	public void updateUserTest() throws ServiceException, DAOException {
		User user = new User(22L, "Jimmy Hendrix", "Jimmy", "pass");
		service.updateUser(user);
		Mockito.verify(userDao, times(1)).update(user);
	}

	@Test
	public void findUserByLoginTest() throws ServiceException, DAOException {
		String login = "Jimmy Hendrix";
		User user = new User(22L, "Jimmy", login, "pass");
		Mockito.when(userDao.fetchByLogin(login)).thenReturn(user);
		User userAfter = service.fetchUserByLogin(login);
		Mockito.verify(userDao, times(1)).fetchByLogin(login);
		assertTrue(user.equals(userAfter));
	}
}
