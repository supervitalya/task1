package com.epam.task1.service;

import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.dao.RoleDAO;
import com.epam.task1.domain.Role;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class RoleServiceImplTest {

	@Mock
	private RoleDAO roleDao;
	@InjectMocks
	@Autowired
	private RoleService service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addRoleTest() throws DAOException, ServiceException {
		Role role = new Role(123L, 321L, "Admin");
		service.addRole(role);
		Mockito.verify(roleDao, times(1)).add(role);
	}

	@Test
	public void removeRoleByUserIdTest() throws DAOException, ServiceException {
		long userId = 33L;
		service.removeRoleByUserId(userId);
		Mockito.verify(roleDao, times(1)).removeByUserId(userId);
	}

}
