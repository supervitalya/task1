package com.epam.task1.service;

import static org.mockito.Mockito.times;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.dao.CommentDAO;
import com.epam.task1.domain.Comment;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceImplTest {

	@Mock
	private CommentDAO dao;
	@InjectMocks
	@Autowired
	private CommentService service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addCommentTest() throws DAOException, ServiceException {
		Comment com = new Comment(1L, 2L, "ASd", new Timestamp(System.currentTimeMillis()));
		service.addComment(com);
		Mockito.verify(dao, times(1)).add(com);
	}

	@Test
	public void removeCommentTest() throws DAOException, ServiceException {
		long commentId = 1;
		service.removeComment(commentId);
		Mockito.verify(dao, times(1)).removeById(commentId);
	}

	@Test
	public void removeAllCommentsTest() throws DAOException, ServiceException {
		long newsId = 99L;
		service.removeAllComments(newsId);
		Mockito.verify(dao, times(1)).removeByNewsId(newsId);
	}

}
