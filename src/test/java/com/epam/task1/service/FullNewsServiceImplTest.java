package com.epam.task1.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.task1.domain.Author;
import com.epam.task1.domain.Comment;
import com.epam.task1.domain.News;
import com.epam.task1.domain.NewsDTO;
import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;
import com.epam.task1.exception.ServiceException;
import com.epam.task1.service.impl.AuthorServiceImpl;
import com.epam.task1.service.impl.CommentServiceImpl;
import com.epam.task1.service.impl.FullNewsServiceImpl;
import com.epam.task1.service.impl.NewsServiceImpl;
import com.epam.task1.service.impl.TagServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)

public class FullNewsServiceImplTest {

	@Mock
	private AuthorServiceImpl authorService;
	@Mock
	private NewsServiceImpl newsService;
	@Mock
	private TagServiceImpl tagService;
	@Mock
	private CommentServiceImpl commentService;

	@InjectMocks
	private FullNewsServiceImpl fullNewsService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void openNewsTest() throws ServiceException, DAOException {
		long newsId = 1L;
		long currentTime = System.currentTimeMillis();
		News news = new News(newsId, "asd", "dsa", "qwe", new Timestamp(currentTime), new Date(currentTime));
		Author author = new Author(1L, "Arnold");
		List<Tag> tags = new ArrayList<>();
		tags.add(new Tag(1L, "tag1"));
		tags.add(new Tag(2L, "tag2"));

		List<Comment> comments = new ArrayList<>();
		comments.add(new Comment(1L, "tag1", new Timestamp(currentTime)));
		comments.add(new Comment(2L, "tag2", new Timestamp(currentTime)));
		NewsDTO ntoBefore = new NewsDTO();
		ntoBefore.setAuthor(author);
		ntoBefore.setCommentsList(comments);
		ntoBefore.setTagsList(tags);
		ntoBefore.setNews(news);

		Mockito.when(newsService.fetchNews(newsId)).thenReturn(news);
		Mockito.when(authorService.fetchByNewsId(newsId)).thenReturn(author);
		Mockito.when(tagService.fetchByNewsId(newsId)).thenReturn(tags);
		Mockito.when(commentService.fetchByNewsId(newsId)).thenReturn(comments);
		NewsDTO ntoAfter = fullNewsService.openNews(newsId);
		assertEquals(ntoBefore, ntoAfter);
	}

	@Test
	public void removeFullNewsTest() throws ServiceException {
		long newsId = 1L;
		fullNewsService.removeFullNews(newsId);
		Mockito.verify(newsService, times(1)).removeNews(newsId);
		Mockito.verify(commentService, times(1)).removeAllComments(newsId);
		Mockito.verify(newsService, times(1)).removeAllTagsFromNews(newsId);
		Mockito.verify(newsService, times(1)).removeAuthorFromNews(newsId);

	}
}
