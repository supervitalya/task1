package com.epam.task1.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.task1.domain.Comment;
import com.epam.task1.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/newsDB.xml")
@DatabaseSetup("/commentDB.xml")
@DatabaseTearDown(value = "/commentDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/newsDB.xml", type = DatabaseOperation.DELETE_ALL)

public class CommentDAOImplTest {
	@Autowired
	CommentDAO commentDao;

	@Test
	public void removeByNewsIdTest() throws DAOException {
		long commentNewsId = 2L;
		assertFalse(commentDao.fetchByNewsId(commentNewsId).isEmpty());
		commentDao.removeByNewsId(commentNewsId);
		assertTrue(commentDao.fetchByNewsId(commentNewsId).isEmpty());
	}

	@Test
	public void addTest() throws DAOException {
		long newsId = 3L;
		Comment com = new Comment(newsId, "new test comment", new Timestamp(System.currentTimeMillis()));
		commentDao.add(com);
		assertTrue(commentDao.fetchByNewsId(newsId).get(0) != null);
	}

	@Test
	public void fetchByNewsIdTest() throws DAOException {
		long newsId = 2L;
		List<Comment> commentsList = commentDao.fetchByNewsId(newsId);
		int sizeBefore = commentsList.size();
		assertEquals(2, sizeBefore);
		commentDao.add(new Comment(newsId, "text", new Timestamp(System.currentTimeMillis())));
		int sizeAfter = commentDao.fetchByNewsId(newsId).size();
		assertEquals(sizeBefore + 1, sizeAfter);
	}
}
