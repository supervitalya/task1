package com.epam.task1.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.task1.domain.Tag;
import com.epam.task1.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/newsDB.xml")
@DatabaseSetup("/tagDB.xml")
@DatabaseSetup("/newsTagDB.xml")
@DatabaseTearDown(value = "/newsTagDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/newsDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/tagDB.xml", type = DatabaseOperation.DELETE_ALL)

public class TagDAOImplTest {
	@Autowired
	TagDAO tagDao;

	@Test
	public void fetchByNewsIdTest() throws DAOException {
		List<Tag> tagsList = tagDao.fetchByNewsId(1L);
		assertFalse(tagsList.isEmpty());
		assertEquals(3, tagsList.size());
		List<Tag> secondTagList = tagDao.fetchByNewsId(3L);
		assertEquals(2, secondTagList.size());
	}

	@Test
	public void fetchAllTest() throws DAOException {
		List<Tag> tagList = tagDao.fetchAll();
		assertEquals(4, tagList.size());
	}

	@Test
	public void addTest() throws DAOException {
		int countBefore = tagDao.fetchAll().size();
		String tagName = "hi-tech";
		Long tagId = tagDao.add(new Tag(tagName));
		int countAfter = tagDao.fetchAll().size();
		assertEquals(countBefore + 1, countAfter);
		Tag tag = tagDao.fetchByName(tagName);
		assertEquals(tagId, tag.getId());
	}
}
