package com.epam.task1.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.task1.domain.User;
import com.epam.task1.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/userDB.xml")
@DatabaseSetup("/roleDB.xml")
@DatabaseTearDown(value = "/roleDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/userDB.xml", type = DatabaseOperation.DELETE_ALL)

public class UserDAOImplTest {
	@Autowired
	UserDAO userDao;

	@Test
	public void fetchByLoginTest() throws DAOException {
		String login = "Brian";
		User user = userDao.fetchByLogin(login);
		assertEquals("Brian May", user.getUserName());
	}

	@Test
	public void addTest() throws DAOException {
		String name = "Mick Jagger";
		User user = new User(name, "Mick", "pass123");
		Long userId = userDao.add(user);
		assertTrue(userId != 0);
		User secondUser = userDao.fetchByLogin("Mick");
		assertEquals(user.getPassword(), secondUser.getPassword());
	}

	@Test
	public void removeByIdTest() throws DAOException {
		String expectedName = "John Lennon";
		User user = new User(expectedName, "Johny", "pass321");
		Long userId = userDao.add(user);
		userDao.removeById(userId);
		User userAfter = userDao.fetchById(userId);
		assertTrue(userAfter == null);
	}

}
