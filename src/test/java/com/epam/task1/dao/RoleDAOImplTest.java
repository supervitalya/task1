package com.epam.task1.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.task1.domain.Role;
import com.epam.task1.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/userDB.xml")
@DatabaseSetup("/roleDB.xml")
@DatabaseTearDown(value = "/roleDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/userDB.xml", type = DatabaseOperation.DELETE_ALL)

public class RoleDAOImplTest {
	@Autowired
	RoleDAO roleDao;

	@Test
	public void fetchByUserIdTest() throws DAOException {
		List<Role> rolesList = roleDao.fetchByUserId(3L);
		String firstExpectedRole = "user";
		String secondExpectedRole = "moderator";
		assertEquals(firstExpectedRole, rolesList.get(0).getRoleName());
		assertEquals(secondExpectedRole, rolesList.get(1).getRoleName());
	}

	@Test
	public void removeByUserIdTest() throws DAOException {
		long userId = 3L;
		List<Role> before = roleDao.fetchByUserId(userId);
		assertEquals(before.size(), 2);
		roleDao.removeByUserId(userId);
		List<Role> after = roleDao.fetchByUserId(userId);
		assertTrue(after.isEmpty());
	}
}
