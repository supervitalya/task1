package com.epam.task1.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.task1.dao.criteria.SearchCriteria;
import com.epam.task1.domain.Author;
import com.epam.task1.domain.News;
import com.epam.task1.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/newsDB.xml")
@DatabaseSetup("/authorDB.xml")
@DatabaseSetup("/tagDB.xml")
@DatabaseSetup("/newsTagDB.xml")
@DatabaseSetup("/newsAuthorDB.xml")

@DatabaseTearDown(value = "/newsAuthorDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/newsTagDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/newsDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/authorDB.xml", type = DatabaseOperation.DELETE_ALL)
@DatabaseTearDown(value = "/tagDB.xml", type = DatabaseOperation.DELETE_ALL)

public class NewsDAOImplTest {
	@Autowired
	NewsDAO newsDao;
	@Autowired
	AuthorDAO authorDao;

	@Test
	public void addTest() throws DAOException {
		long countBefore = newsDao.countNews();
		System.out.println(countBefore);
		News news = new News("News1", "Murder", "Killed John Kennedy", new Timestamp(System.currentTimeMillis()),
				new Date(System.currentTimeMillis()));
		newsDao.add(news);
		long countAfter = newsDao.countNews();
		System.out.println(countAfter);
		assertEquals(countBefore + 1, countAfter);
	}

	@Test
	public void fetchAllTest() throws DAOException {
		List<News> newsList = newsDao.fetchAll();
		assertFalse(newsList.isEmpty());
		newsDao.add(new News("News2", "Holidays", "Holidays in belarus", new Timestamp(System.currentTimeMillis()),
				new Date(System.currentTimeMillis())));
		List<News> newsAfterAdding = newsDao.fetchAll();
		assertEquals(newsList.size() + 1, newsAfterAdding.size());
	}

	@Test
	public void updateTest() throws DAOException {
		String title = "Test news title";
		News news = newsDao.fetchById(1L);
		String titleBefore = news.getTitle();
		news.setTitle(title);
		newsDao.update(news);
		String titleAfter = newsDao.fetchById(1L).getTitle();
		assertFalse(titleAfter.equals(titleBefore));
		assertEquals(title, titleAfter);
	}

	@Test
	public void removeByIdTest() throws DAOException {
		long newsId = 2L;
		News news = newsDao.fetchById(newsId);
		assertNotNull(news);
		newsDao.removeTagRelationsByNewsId(newsId);
		newsDao.removeById(newsId);
		News nullableNews = newsDao.fetchById(newsId);
		assertNull(nullableNews);
	}

	@Test
	public void countNewsTest() throws DAOException {
		long newsId = 1L;
		long countBefore = newsDao.countNews();
		assertFalse(countBefore == 0);
		newsDao.removeTagRelationsByNewsId(newsId);
		newsDao.removeById(newsId);
		long countAfter = newsDao.countNews();
		assertEquals(countAfter + 1, countBefore);
	}

	@Test
	public void fetchByCriteriaTest() throws DAOException {
		long authorId = 6L;
		long expectedNewsId = 3L;
		SearchCriteria criteriaAuthor = new SearchCriteria();
		List<Long> authorsId = new ArrayList<>();
		authorsId.add(authorId);
		criteriaAuthor.setAuthorsIdList(authorsId);
		List<Long> tagsId = new ArrayList<>();
		tagsId.add(1L);
		tagsId.add(4L);
		criteriaAuthor.setTagsIdList(tagsId);
		List<News> newsList = newsDao.fetchByCriteria(criteriaAuthor);
		assertFalse(newsList.isEmpty());
		News actual = newsList.get(0);
		assertTrue(expectedNewsId == actual.getId());

	}

	@Test
	public void addAuthorToNewsTest() throws DAOException {
		long curTime = System.currentTimeMillis();
		News news = new News("text", "text", "text", new Timestamp(curTime), new Date(curTime));
		long newsId = newsDao.add(news);
		news.setId(newsId);
		Author author = new Author("George");
		long authorId = authorDao.add(author);
		newsDao.addAuthorToNews(newsId, authorId);
		SearchCriteria sc = new SearchCriteria();
		List<Long> authorsId = new ArrayList<>();
		authorsId.add(authorId);
		sc.setAuthorsIdList(authorsId);
		List<News> newsList = newsDao.fetchByCriteria(sc);
		assertFalse(newsList.isEmpty());
		News newsActual = newsList.get(0);
		assertTrue(news.getId().equals(newsActual.getId()));
	}

}
