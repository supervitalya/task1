package com.epam.task1.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.task1.domain.Author;
import com.epam.task1.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/authorDB.xml")
@DatabaseTearDown(value = "/authorDB.xml", type = DatabaseOperation.DELETE_ALL)

public class AuthorDAOImplTest {

	@Autowired
	private AuthorDAO authorDao;

	@Test
	public void addTest() throws DAOException {
		String name = "Valentino Rossi";
		Author author = new Author(name);
		long id = authorDao.add(author);
		Author createdAuthor = authorDao.fetchById(id);
		Assert.assertEquals(author.getAuthorName(), createdAuthor.getAuthorName());
	}

	@Test
	public void fetchAllTest() throws DAOException {
		List<Author> authors = authorDao.fetchAll();
		Author a = new Author("Ozzy Osbourne");
		authorDao.add(a);
		List<Author> authorsPlusOneMore = authorDao.fetchAll();
		assertEquals(authors.size() + 1, authorsPlusOneMore.size());
		assertEquals(7, authorsPlusOneMore.size());
	}

	@Test
	public void updateTest() throws DAOException {
		long authorId = 1L;
		Author author = authorDao.fetchById(authorId);
		String nameBefore = author.getAuthorName();
		author.setAuthorName("New Name");
		authorDao.update(author);
		Author authorAfter = authorDao.fetchById(authorId);
		Assert.assertNotEquals(nameBefore, authorAfter.getAuthorName());
	}

	@Test
	public void removeByIdTest() throws DAOException {
		List<Author> before = authorDao.fetchAll();
		Author existingAuthor = authorDao.fetchById(1L);
		assertTrue(existingAuthor != null);
		authorDao.removeById(1L);
		List<Author> after = authorDao.fetchAll();
		assertEquals(before.size() - 1, after.size());
		Author deletedAuthor = authorDao.fetchById(1L);
		assertTrue(deletedAuthor == null);
	}

	@Test
	public void fetchNotExpiredTest() throws DAOException {
		List<Author> all = authorDao.fetchAll();
		List<Author> notExpired = authorDao.fetchNotExpired();
		notExpired.forEach((author) -> assertNull(author.getExpired()));
		all.removeAll(notExpired);
		all.forEach((author) -> assertNotNull(author.getExpired()));
	}
}
